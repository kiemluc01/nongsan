from rest_framework import serializers
from shop.models import *

class TinhSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tinh
        fields = '__all__'
        
class HuyenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Huyen
        fields = '__all__'

class PhuongXaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhuongXa
        fields = '__all__'
        
class NhacungCapSerializer(serializers.ModelSerializer):
    class Meta:
        model = NhaCungCap
        fields = '__all__'
        
class PhieuNhapSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhieuNhap
        fields = '__all__'
        
class chitietPhieuNhapSerializer(serializers.ModelSerializer):
    class Meta:
        model = chitietPhienhap
        fields = '__all__'

class HoaDonSerializer(serializers.ModelSerializer):
    class Meta:
        model = HoaDon
        fields = '__all__'

class ChiTietHoaDonSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChitietHoaDon
        fields = '__all__'
        
class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Anh
        fields = '__all__'
class SanPhamSerializer(serializers.ModelSerializer):
    anh = ImageSerializer(read_only=True, many=True)
    class Meta:
        model = SanPham
        fields = '__all__'
        
class DanhMucSerializer(serializers.ModelSerializer):
    class Meta:
        model = DanhMuc
        fields = '__all__'
        
        
class KhachhangSerializer(serializers.ModelSerializer):
    class Meta:
        model = KhachHang
        fields = '__all__'
        
class ProfileSerializer(serializers.ModelSerializer):
    user = KhachhangSerializer(read_only=True)
    class Meta:
        model = User
        fields = ['username', 'password', 'user']