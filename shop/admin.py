from django.contrib import admin
from shop.models import Tinh, Huyen, PhieuNhap, PhuongXa, SanPham, chitietPhienhap, HoaDon, ChitietHoaDon, KhachHang, Anh, \
    NhaCungCap, DanhMuc

# Register your models here.
admin.site.register(Tinh)
admin.site.register(Huyen)
admin.site.register(PhieuNhap)
admin.site.register(PhuongXa)
admin.site.register(SanPham)
admin.site.register(chitietPhienhap)
admin.site.register(HoaDon)
admin.site.register(ChitietHoaDon)
admin.site.register(KhachHang)
admin.site.register(Anh)
admin.site.register(NhaCungCap)
admin.site.register(DanhMuc)