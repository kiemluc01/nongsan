from django.urls import path, include
from rest_framework.routers import DefaultRouter
from shop import views

router =  DefaultRouter()
router.register(r'danhmuc', views.DanhMucViewset, basename="Danh_muc")
router.register(r'sanpham', views.SanPhamViewset, basename="san_pham")
router.register(r'tinh', views.TinhViewset, basename="Tinh")
router.register(r'huyen', views.HuyenViewset, basename="huyen")
router.register(r'phuong_xa', views.PhuongXaViewset, basename="phuong_xa")
router.register(r'hoa_don', views.HoaDonViewset, basename="hoa_don")
router.register(r'phieu_nhap', views.PhieuNhapViewset, basename="phieu_nhap")
router.register(r'chitiet_hoadon', views.chitietHoadonViewset, basename="chitiet_hoadon")
router.register(r'chi_tiet_phieu_nhap', views.ChitietPhieunhapViewset, basename="chi_tiet_phieu_nhap")
router.register(r'nha_cung_cap', views.NhaCungCapViewset, basename="nha_cung_cap")
router.register(r'hinh_anh', views.HinhAnhViewset, basename="hinh_anh")

urlpatterns = router.urls + [
    path('profile/', views.ProfileAPIView.as_view(), name="profile"),
    path('register/', views.Register.as_view(), name="register"),
]
