from django.core.management.base import BaseCommand
from shop.models import Tinh, PhuongXa, Huyen
from shop.serializers import TinhSerializer, HuyenSerializer, PhuongXaSerializer
from django.templatetags.static import static
import json
class Command(BaseCommand):
    help = 'Import provinces from xlsx'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        with open('shop/{}'.format(static("provinces.json")),encoding='utf-8') as file:
            provinces = json.load(file)
        for province in provinces:
            print('creating province {}'.format(province['name']))
            if not Tinh.objects.filter(name=province["name"]).exists():
                province_row = Tinh.objects.create(name=province["name"])
                province_row.save()
            for district in province['districts']:
                print('creating district {}'.format(district['name']))
                province_row = Tinh.objects.filter(name=province["name"]).first()
                if not Huyen.objects.filter(name=district["name"]).exists():
                    district_row = Huyen.objects.create(name=district["name"], tinh=province_row)
                    district_row.save()
                for ward in district['wards']:
                    print('creating ward {}'.format(ward['name']))
                    district_row = Huyen.objects.filter(name=district["name"]).first()
                    if not PhuongXa.objects.filter(name=ward["name"]).exists():
                        ward_row = PhuongXa.objects.create(name=ward["name"], huyen=district_row)
                        ward_row.save()