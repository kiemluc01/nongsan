from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class Tinh(models.Model):
    name = models.CharField("Tên Tỉnh", max_length=100, null=True, blank=True)
    
    def __str__(self):
        return self.name

class Huyen(models.Model):
    name = models.CharField("Tên Huyện", max_length=150)
    tinh = models.ForeignKey("shop.Tinh", related_name="tinh", on_delete=models.CASCADE)
    
class PhuongXa(models.Model):
    name  = models.CharField("Tên phường xã", max_length=150)
    huyen = models.ForeignKey("shop.Huyen", related_name="huyen", on_delete=models.CASCADE)
    
class NhaCungCap(models.Model):
    name = models.CharField("Tên Nhà cung cấp", max_length=150)
    diachi = models.CharField("địa chỉ", max_length=255)
    sdt = models.CharField("SĐT", max_length=10)
    
class KhachHang(models.Model):
    user = models.OneToOneField (User, related_name="user", on_delete=models.CASCADE)
    tenKH = models.CharField("Tên KH", max_length=150, null=True)
    sdt = models.CharField("SDT", max_length=10, null=True)
    ngaysinh = models.DateField("Ngày Sinh", null=True)
    cccd = models.CharField("CCCD", max_length=50, null=True)
    gioitinh = models.CharField("Giới Tính", max_length=5, null=True)
    diachi = models.CharField("Địa Chỉ", max_length=254, null=True)
    
class DanhMuc(models.Model):
    ten = models.CharField("Tên", max_length=150)
class SanPham(models.Model):
    ten =  models.CharField("Tên SP", max_length=150)
    rootImage = models.CharField( max_length=254, null=True)
    mota = models.CharField("Mô Tả", max_length=1000)
    giatien = models.IntegerField("Giá", default=0)
    danhmuc = models.ForeignKey("shop.DanhMuc", related_name="danhmuc", on_delete=models.CASCADE)
    soluong = models.IntegerField("Số lượng còn")
    
class Anh(models.Model):
    sanpham = models.ForeignKey("shop.SanPham", related_name="anh", on_delete=models.CASCADE)
    anh = models.ImageField("Image", upload_to="", height_field=None, width_field=None, max_length=None)
    
class HoaDon(models.Model):
    user = models.ForeignKey(User, related_name="user_order", null=True,on_delete=models.CASCADE)
    ten_nhan_hang = models.CharField(max_length=150, null=True)
    ngaydat = models.DateField("Ngày Đặt", default=timezone.now)
    thanhtien = models.IntegerField("Thành Tiền", default=0)
    diachidathang = models.CharField("Đại chỉ đặt hàng", max_length=255, null=True)
    sdtgiaohang = models.CharField("SDT", max_length=10, null=True)
    trangthai = models.CharField("Trạng thái", max_length=50)

class ChitietHoaDon(models.Model):
    hoadon = models.ForeignKey("shop.HoaDon", related_name="hoadon", on_delete=models.CASCADE)
    sanpham = models.ForeignKey("shop.SanPham", related_name="sanpham", on_delete=models.CASCADE)
    soluong = models.IntegerField("số lượng", default=0)
    dongia = models.IntegerField("Đơn Giá", default=0)
    
class PhieuNhap(models.Model):
    nhacungcap = models.ForeignKey("shop.NhaCungCap", related_name="nhacungcap", on_delete=models.CASCADE)
    ngaynhap = models.DateField("Ngày nhập", default=timezone.now)

class chitietPhienhap(models.Model):
    phieunhap = models.ForeignKey("shop.PhieuNhap", related_name="phieunhap", on_delete=models.CASCADE)
    sanpham = models.ForeignKey("shop.SanPham", related_name="sanpham_phieunhap", on_delete=models.CASCADE)
    soluongnhap = models.IntegerField("Số Lượng", default=0)
    gianhap = models.IntegerField("Giá Nhập", default=0)