# Generated by Django 4.2.1 on 2023-05-21 06:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shop', '0006_alter_khachhang_cccd_alter_khachhang_diachi_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='khachhang',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL),
        ),
    ]
