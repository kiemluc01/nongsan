# Generated by Django 4.2 on 2023-05-01 10:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_alter_khachhang_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='khachhang',
            old_name='ten',
            new_name='tenKH',
        ),
        migrations.RemoveField(
            model_name='khachhang',
            name='email',
        ),
    ]
