# Generated by Django 4.2.1 on 2023-05-28 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0008_alter_khachhang_ngaysinh'),
    ]

    operations = [
        migrations.AddField(
            model_name='hoadon',
            name='ten_dat_hang',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
