from django.shortcuts import render
from rest_framework import viewsets, status, views
from rest_framework.response import Response
from shop.serializers import *
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
import django_filters

class DanhMucViewset(viewsets.ModelViewSet):
    queryset = DanhMuc.objects.all()
    serializer_class = DanhMucSerializer
    permission_classes = [AllowAny,]
    
class SanPhamFilter(django_filters.FilterSet):
    ten = django_filters.CharFilter(field_name='ten', lookup_expr="icontains")
class SanPhamViewset(viewsets.ModelViewSet):
    queryset = SanPham.objects.all()
    serializer_class = SanPhamSerializer
    filterset_class = SanPhamFilter
    permission_classes = [AllowAny,]
    
class TinhViewset(viewsets.ModelViewSet):
    queryset = Tinh.objects.all()
    serializer_class = TinhSerializer
    permission_classes = [AllowAny,]
    
class HuyenViewset(viewsets.ModelViewSet):
    queryset = Huyen.objects.all()
    serializer_class = HuyenSerializer
    permission_classes = [AllowAny,]
    
class PhuongXaViewset(viewsets.ModelViewSet):
    queryset = PhuongXa.objects.all()
    serializer_class =  PhuongXaSerializer
    permission_classes = [AllowAny,]
    
class NhaCungCapViewset(viewsets.ModelViewSet):
    queryset = NhaCungCap.objects.all()
    serializer_class = NhacungCapSerializer
    permission_classes = [AllowAny,]
    
class PhieuNhapViewset(viewsets.ModelViewSet):
    queryset = PhieuNhap.objects.all()
    serializer_class = PhieuNhapSerializer
    permission_classes = [AllowAny,]
    
class ChitietPhieunhapViewset(viewsets.ModelViewSet):
    queryset = chitietPhienhap.objects.all()
    serializer_class = chitietPhieuNhapSerializer
    permission_classes = [AllowAny,]
    

class HoaDonViewset(viewsets.ModelViewSet):
    queryset = HoaDon.objects.all()
    serializer_class = HoaDonSerializer
    permission_classes = [AllowAny,]
    
    @action(detail=False, methods=['post'])
    def create_order(self, request):
        hoadon = HoaDon(user=request.user, trangthai="chưa xác nhận", ten_nhan_hang=request.data['full_name'], diachidathang=request.data['addr'], sdtgiaohang=request.data['sdtgiaohang'])
        hoadon.save()
        items = request.data["sanpham"]
        for item in items:
            sanpham = SanPham.objects.get(pk=item["id"])
            chitiet_donhang = ChitietHoaDon(hoadon=hoadon,sanpham=sanpham, soluong=item["soluong"])
            chitiet_donhang.save()
            
        return Response(request.data, status=status.HTTP_201_CREATED)
    
    @action(detail=True, methods=['get'])
    def capnhat_trangthai(self, request):
        hoadon = self.get_object()
        hoadon.trangthai = "Đã xác nhận"
        hoadon.save()
        return Response(HoaDonSerializer(hoadon).data, status=status.HTTP_200_OK) 
    
class chitietHoadonViewset(viewsets.ModelViewSet):
    queryset = ChitietHoaDon.objects.all()
    serializer_class = ChiTietHoaDonSerializer
    permission_classes = [AllowAny,]
    
class ProfileAPIView(views.APIView):
    permission_classes = [AllowAny,]
    
    def get(self, request):
        user =  request.user
        print(user)
        serializer = ProfileSerializer(user)
        return  Response(serializer.data)
    
class HinhAnhViewset(viewsets.ModelViewSet):
    queryset = Anh.objects.all()
    serializer_class = ImageSerializer
    permission_classes = [AllowAny,]
    
class Register(views.APIView):
    
    permission_classes = [AllowAny]
    
    def post(self, request):
        print(request.data["username"])
        if User.objects.filter(username=request.data["username"]).exists():
            return Response({"detail":"username is already"}, status=status.HTTP_400_BAD_REQUEST)
        user = User.objects.create_user(username=request.data["username"], password=request.data["password"], is_active=True, is_staff=True)
        user.save()
        khachhang = KhachHang(user=user)
        khachhang.save()
        return Response(ProfileSerializer(user).data, status=status.HTTP_201_CREATED)
    

        